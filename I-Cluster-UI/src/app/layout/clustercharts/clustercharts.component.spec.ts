import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterchartsComponent } from './clustercharts.component';

describe('ClusterchartsComponent', () => {
  let component: ClusterchartsComponent;
  let fixture: ComponentFixture<ClusterchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClusterchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
