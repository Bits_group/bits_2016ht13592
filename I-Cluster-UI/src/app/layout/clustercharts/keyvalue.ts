export class KeyValue {
    key: string;
    value: string;   
  
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }