import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from "@angular/router";
import { ClusterchartsComponent } from './clustercharts/clustercharts.component';
import { InflowComponent } from './inflow/inflow.component';
import { UndefinedComponent } from './undefined/undefined.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
   ClusterchartsComponent,
   InflowComponent,
   UndefinedComponent,
  ],
  exports : [
  ]
})
export class LayoutModule { }
