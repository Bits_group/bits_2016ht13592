import { KeyValue } from './keyvalue';

export class ClusterInfo {
    chartLabel: string;
    chartValueList: KeyValue[];   
  
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }