import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ClusterInfo } from './clusterinfo';
import { ClusterInfoService } from '../service/cluster.service';


@Component({
  selector: 'app-inflow',
  templateUrl: './inflow.component.html',
  styleUrls: ['./inflow.component.css']
})
export class InflowComponent implements OnInit {

  chart = [];
  private clusterInfo : ClusterInfo;
  private labelsFromService = [];
  private dataFromService = [];
  constructor(private clusterInfoService: ClusterInfoService) { }

  ngOnInit() {

    this.clusterInfoService.getClusterInfo()
    .subscribe(
    restItems => { 
      this.clusterInfo = restItems;  
      //console.log(this.clusterInfo);
      this.clusterInfo.chartValueList.forEach( e => {
        this.labelsFromService.push('cluster ' + e.key);
        this.dataFromService.push(e.value);
      });


      this.chart = new Chart('canvas', {
        type: 'bar',
        data: {
          //labels: ["Infra", "Reports", "Payments", "Bonus", "UI", "LDAP", "Build", "SQL", "NOSQL", "Customer", "Inventory", "Checkoout"],
          labels : this.labelsFromService,
          datasets: [{
            label: this.clusterInfo.chartLabel,
            data: this.dataFromService,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
        },
        options: {
          responsive: false,
          scales: {
            xAxes: [{
              ticks: {
                maxRotation: 90,
                minRotation: 80
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

    });




  }

}
