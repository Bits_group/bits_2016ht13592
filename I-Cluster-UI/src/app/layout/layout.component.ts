import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TileService } from './service/tile.service';
import { Tile } from '../model/Tile';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  private tiles: Tile[][];
  private subscription;

  constructor(private tileService: TileService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    // this.subscription = this.route
    //   .queryParams
    //   .subscribe(v => { 
    //     this.execute(v); 
    //   }); 
  }

  ngOnDestroy() {
    //this.subscription.unsubscribe();
  }

  execute(routerParam) {
    if (routerParam.id == 'student') {
      this.tileService.ssServiceGetRestItems()
        .subscribe(
        restItems => {
          this.tiles = this.partition(restItems, 4);
          console.log(this.tiles[0][3].tileIcon);
        });
    } else {
      this.tileService.getTiles(
        routerParam.id == null ? 'home' : routerParam.id,
        routerParam.category == null ? 'self' : routerParam.category,
        routerParam.role == null ? 'unknown' : routerParam.role)
        .subscribe(
        restItems => {
          this.tiles = this.partition(restItems, 4);
          console.log(this.tiles[0][3].tileIcon);
        });
    }
  }

  partition(array, splitSize) {
    var groups = [];
    for (var groupIndex = 0, numGroups = Math.ceil(array.length / splitSize);
      groupIndex < numGroups;
      groupIndex++) {
      var startIndex = groupIndex * splitSize;
      groups.push(array.slice(startIndex, startIndex + splitSize));
    }
    return groups;
  }

}
