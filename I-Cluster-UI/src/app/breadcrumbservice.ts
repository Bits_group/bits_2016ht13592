import { Component, OnInit, Output, EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class BreadCrumbService {

  isOpen = false;

  @Output() change: EventEmitter<string []> = new EventEmitter();

  update(stringArray) {
        this.change.emit(stringArray);
  }

}