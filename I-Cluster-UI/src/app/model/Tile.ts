export class Tile {
    tileName: string;
    text: string;
    tileColor: string;
    tileIcon: string;
  
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }