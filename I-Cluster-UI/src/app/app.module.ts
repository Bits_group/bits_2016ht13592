import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, Routes, RouterModule, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from 'rxjs';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent } from './footer/footer.component';

import { HttpClientModule } from '@angular/common/http';
import { HeaderModule } from './header/header.module';
import { LayoutModule} from './layout/layout.module'
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main.component';
import { ClusterchartsComponent } from 'src/app/layout/clustercharts/clustercharts.component';
import { InflowComponent } from 'src/app/layout/inflow/inflow.component';
import { UndefinedComponent } from 'src/app/layout/undefined/undefined.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptor';
import { BreadCrumbService } from './breadcrumbservice';

class Permissions {
  canGoToRoute(id: string): boolean { 
       return true;
  }
}

@Injectable()
class AuthGuard implements CanActivateChild, CanActivate {
  constructor(private permissions: Permissions, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      let isLoggedIn : boolean = true;
      if(localStorage.getItem('userId') == 'null'){      
        isLoggedIn = false;
      }
      if(isLoggedIn || state.url == 'login') {
        return this.permissions.canGoToRoute(state.url);
      } else {
        this.router.navigate(['login']);
        return false;
      }
    
  }


  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    //console.log(state.url);
    let isLoggedIn : boolean = true;
    if(localStorage.getItem('userId') == 'null'){      
      isLoggedIn = false;
    }
    if(isLoggedIn || state.url == 'login') {
      return this.permissions.canGoToRoute(state.url);
    } else {
      this.router.navigate(['login']);
      return false;
    }   
    
  }
}


const routes: Routes = [ 
  { path: 'home', 
    component: MainComponent,
    canActivateChild: [AuthGuard],
    children: [ 
      {path: '', component: LayoutComponent, data : {id : 'anythng'}},       
      {path: 'charts/cluster', component: ClusterchartsComponent, data : {id : 'clusterinfo'}},  
      {path: 'charts/inflow', component: InflowComponent, data : {id : 'inflowdata'}}      
    ]
  },
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'undefined', canActivate: [AuthGuard], component: UndefinedComponent, data : {id : 'Undefined'} },
  { path: '**',  redirectTo: 'undefined', pathMatch: 'full'}
  
 ];


@NgModule({
  imports: [   
    BrowserModule,
    HttpClientModule,
    HeaderModule,
    LayoutModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(routes, {useHash: true}),      
  ],
  declarations: [
    AppComponent,
    LayoutComponent,
    FooterComponent,
    LoginComponent,
    MainComponent
  ],
  providers: [Permissions, AuthGuard , 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }, BreadCrumbService 
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
