import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { BreadCrumbService } from './breadcrumbservice';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private router: Router, private breadCrumbService: BreadCrumbService) { }
  private linkpath : string[] = new Array();

  ngOnInit() {
    this.linkpath.push('Home');
    this.breadCrumbService.change.subscribe(array => {
      this.linkpath = array;
    });
  
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
  ).subscribe((route: ActivatedRoute) => {
     // console.log(route);     
  });
  }

}
