import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { filter, scan } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { catchError, tap }  from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions  } from '@angular/http';



const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
 
  public getClasses() : Observable<any> {
    return this.http
      .get(API_URL+'util/getClasses');
  }

  public getDivisions(clazz:string) : Observable<any> {
    return this.http
      .get(API_URL+'util/getDivisions?class='+clazz);
  }

}
