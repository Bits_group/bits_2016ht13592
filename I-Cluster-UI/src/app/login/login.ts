export class Login {
    image: string;
    expires: string;   
    permissions: string;
    token: string;
    
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }