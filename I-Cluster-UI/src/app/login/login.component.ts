import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Login } from './login';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  private loginResponse : Login;
  private userId  : string;
  private password : string;
 

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    
  }

  login() {
    this.loginService.login(this.userId, this.password).subscribe(
    restItems => { 
      this.loginResponse = restItems;  
      localStorage.setItem('token', this.loginResponse.token);
      localStorage.setItem('userId', this.userId);
      this.userId = null;
      this.password = null;
      this.router.navigate(['home']); 
    },
    (err) => {
      this.userId = null;
      this.password = null;
      console.log(err);
    }
    );
    
  }


}
