import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { filter, scan } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { catchError, tap }  from 'rxjs/operators';
import { Login } from './login';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions  } from '@angular/http';
import {HttpHeaders} from '@angular/common/http'


const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }


  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

 
  login(userName : string, password : string) : Observable<Login> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'      
    });
   
    return this.http
      .post<Login>(API_URL+'api/login', {'userName' : userName, 'password': password}, { headers: headers });
  }  

}
