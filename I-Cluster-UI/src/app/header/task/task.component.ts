import { Component, OnInit } from '@angular/core';
import { TaskService } from './task.service';
import { Tasks } from './tasks';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  private tasks : Tasks[] = new Array();
  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.taskService.getTasks()
    .subscribe(
    restItems => { 
      this.tasks = restItems;  
    });
  }

}
