import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { filter, scan } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { catchError, tap }  from 'rxjs/operators';
import { Tasks } from './tasks';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions  } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { RequestOptionsArgs } from '@angular/http/src/interfaces';


const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
 
  getTasks() : Observable<Tasks[]> {
    const headerss = new Headers();
    headerss.append('Content-Type', 'application/json');
    headerss.append('authentication', 'hello');

    const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem('token') }) };
    return this.http
      .get<Tasks[]>((API_URL+'api/dashboard/getTaskProgress'), options);
  }  

}
