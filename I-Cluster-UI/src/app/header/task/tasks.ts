export class Tasks {
    taskName: string;
    taskProgress: string;   
  
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }