export class Alert {
    alertName: string;
    alertTime: string;   
    alertDesc: string;
    
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }