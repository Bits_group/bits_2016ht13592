import { Component, OnInit } from '@angular/core';
import { AlertService } from './alert.service';
import { Alert } from './alert';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  private alerts: Alert[] = new Array();
  constructor(private alertService : AlertService) { }

  ngOnInit() {
    this.alertService.getAlerts()
    .subscribe(
    restItems => { 
      this.alerts = restItems;  
    });
  }

}
