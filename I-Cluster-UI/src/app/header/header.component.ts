import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { BreadCrumbService } from 'src/app/breadcrumbservice';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private activeItem : string = 'home';
  private array : string[] = new Array();

  constructor(private router: Router, private breadCrumbService: BreadCrumbService) {    
  }

  ngOnInit() {
  }

  onSelfClick(){    
    this.router.navigate(['home'], {queryParams : {id:'home', category:'self', role:'p'}});
  }

  onManageClick(){
    this.router.navigate(['home'], {queryParams : {id:'home', category:'manage', role:'p'}});
  }

  onConfigureClick(): void{
    this.router.navigate(['home'], {queryParams : {id:'home', category:'configure', role:'p'}});
  }

  setActive(item) : void{
    this.activeItem  = item;

    if(item === 'home') {
      if(this.array.indexOf('Home') == -1){
        this.array.push('Home')
      } else{
        this.array.splice(1);
      }
    } else{
      if(this.array.indexOf('Home') == -1){
        this.array.push('Home')
      }
      if(this.array.indexOf(item) == -1){
        this.array.push(item);
      }
    }
    this.breadCrumbService.update(this.array);
  }

}
