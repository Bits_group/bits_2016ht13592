import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { AlertComponent } from './alert/alert.component';
import { TaskComponent } from './task/task.component';
import { NotificationComponent } from './notification/notification.component';
import { HeaderComponent } from './header.component';

@NgModule({  
  imports: [
    CommonModule    
  ],
  declarations: [
    ProfileComponent,
    AlertComponent,
    TaskComponent,
    NotificationComponent,    
    HeaderComponent
  ],
  exports: [
    ProfileComponent,
    AlertComponent,
    TaskComponent,
    NotificationComponent,   
    HeaderComponent
  ]
})
export class HeaderModule { }
