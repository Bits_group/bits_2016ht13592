export class Notification {
    notificationName: string;
    notificationDate: string;   
  
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }