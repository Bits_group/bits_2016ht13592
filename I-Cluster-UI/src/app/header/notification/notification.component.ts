import { Component, OnInit } from '@angular/core';
import { NotificationService } from './notification.service';
import { Notification } from './notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  private notifications : Notification[] = new Array();
  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.getNotifications()
    .subscribe(
    restItems => { 
      this.notifications = restItems;  
    });
  }

}
