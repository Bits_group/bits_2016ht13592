import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { filter, scan } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { catchError, tap }  from 'rxjs/operators';
import { Notification } from './notification';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions  } from '@angular/http';
import { HttpHeaders } from '@angular/common/http'

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }


  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

 
  getNotifications() : Observable<Notification[]> {
    let headers = new HttpHeaders();
    headers.append('Papap', 'Bearer tok');
    return this.http
      .get<Notification[]>(API_URL+'api/dashboard/getNotifications', {headers});
  }  

}
