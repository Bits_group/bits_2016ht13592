package org.bits.common.constants;

public enum BusinessConstants {

	UPDATED_SUCCESSFULLY("Updated successfully");
	
	private String message;

	private BusinessConstants(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
