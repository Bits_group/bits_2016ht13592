package org.bits.dashboard.service;

import javax.inject.Inject;

import org.bits.dashboard.dao.DashboardDao;
import org.bits.dashboard.dto.ChartDataDto;
import org.springframework.stereotype.Component;

@Component
public class DashboardService {
	private DashboardDao dashboardDao;
	
	@Inject
	public DashboardService(DashboardDao dashboardDao) {
		this.dashboardDao = dashboardDao;
	}
	public ChartDataDto getChartData() {
		return this.dashboardDao.getChartData();
	}
}
