package org.bits.dashboard.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.bits.dashboard.dto.ChartDataDto;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms.Bucket;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class DashboardDaoImpl implements DashboardDao {

	private ElasticsearchOperations elasticTemplate;

	@Inject
	public DashboardDaoImpl(ElasticsearchOperations elasticTemplate) {
		this.elasticTemplate = elasticTemplate;
	}

	public ChartDataDto getChartData() {
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices("bits").withTypes("cluster")
				.addAggregation(AggregationBuilders.global("agg")
						.subAggregation(AggregationBuilders.terms("agg_name").field("cluster")))
				.build();
		Aggregations aggregations = elasticTemplate.query(searchQuery, new ResultsExtractor<Aggregations>() {
			@Override
			public Aggregations extract(SearchResponse response) {
				return response.getAggregations();
			}
		});

		org.elasticsearch.search.aggregations.bucket.global.InternalGlobal ig = aggregations.get("agg");
		Aggregations internalAggregations = ig.getAggregations();

		Iterator<Aggregation> itr = internalAggregations.iterator();
		Map<String, String> chartDataMap = new HashMap<>();
		while (itr.hasNext()) {
			LongTerms term = (LongTerms) itr.next();
			List<Bucket> bucketList = term.getBuckets();
			Iterator<Bucket> itrBucket = bucketList.iterator();
			while (itrBucket.hasNext()) {
				Bucket bucket = itrBucket.next();
				chartDataMap.put(bucket.getKeyAsString(), String.valueOf(bucket.getDocCount()));
			}
		}
		
		ChartDataDto chartDataDto = new ChartDataDto();
		chartDataDto.setChartLabel("Incident Cluster");
		chartDataDto.setClusterMap(chartDataMap);
		return chartDataDto;
		// new DashboardResultsExtractor<Person>()
	}

	class DashboardResultsExtractor<T> implements ResultsExtractor<List<T>> {
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<T> typeReference = new TypeReference<T>() {
		};

		@Override
		public List<T> extract(SearchResponse response) {
			List<T> list = new ArrayList<>();
			try {
				for (SearchHit hit : response.getHits()) {
					if (hit != null) {
						T t = mapper.readValue(hit.getSourceAsString(), typeReference);
						list.add(t);
					}
				}
			} catch (IOException e) {

			}
			return list;
		}

	}

}
