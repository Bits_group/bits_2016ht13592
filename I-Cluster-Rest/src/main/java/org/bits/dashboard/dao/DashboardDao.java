package org.bits.dashboard.dao;

import org.bits.dashboard.dto.ChartDataDto;

public interface DashboardDao {
	ChartDataDto getChartData();
}
