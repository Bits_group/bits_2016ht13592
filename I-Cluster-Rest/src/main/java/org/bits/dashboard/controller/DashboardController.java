package org.bits.dashboard.controller;

import java.util.List;

import org.bits.dashboard.dto.ChartModel;
import org.bits.util.controller.dto.Alert;
import org.bits.util.controller.dto.Notification;
import org.bits.util.controller.dto.Task;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/dashboard")
public interface DashboardController {
	@RequestMapping(value = "/getTaskProgress", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Returns task progress list", notes = "Returns task progress list", response = List.class)
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "success", response = List.class),
	    @ApiResponse(code = 404, message = "Bad request"),
	    @ApiResponse(code = 500, message = "Internal server error")}
	)
	public ResponseEntity<List<Task>> getTaskProgress();	
	
	@RequestMapping(value = "/getNotifications", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Returns notification list", notes = "Returns notification list", response = List.class)
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "success", response = List.class),
	    @ApiResponse(code = 404, message = "Bad request"),
	    @ApiResponse(code = 500, message = "Internal server error")}
	)
	public ResponseEntity<List<Notification>> getNotifications();
	
	
	@RequestMapping(value = "/getAlerts", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "Returns alerts list", notes = "Returns alerts list", response = List.class)
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "success", response = List.class),
	    @ApiResponse(code = 404, message = "Bad request"),
	    @ApiResponse(code = 500, message = "Internal server error")}
	)
	public ResponseEntity<List<Alert>> getAlerts();
	
		
	@GetMapping(path = "/chart")
	public ResponseEntity<ChartModel> getChart();
}
