package org.bits.dashboard.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.bits.dashboard.dto.ChartDataDto;
import org.bits.dashboard.dto.ChartModel;
import org.bits.dashboard.dto.ChartValue;
import org.bits.dashboard.service.DashboardService;
import org.bits.util.controller.dto.Alert;
import org.bits.util.controller.dto.Notification;
import org.bits.util.controller.dto.Task;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DashboardControllerImpl implements DashboardController {

	private DashboardService dashboardService;

	@Inject
	public DashboardControllerImpl(DashboardService service) {
		this.dashboardService = service;
	}

	@Override
	public ResponseEntity<List<Task>> getTaskProgress() {
		List<Task> list = new ArrayList<>();
		list.add(new Task("Task 1", "30"));
		list.add(new Task("Task 2", "60"));
		list.add(new Task("Task 3", "90"));
		list.add(new Task("Task 4", "10"));
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Notification>> getNotifications() {
		List<Notification> list = new ArrayList<>();
		list.add(new Notification("Jenkins build failed - Product adapter", "11:34:45"));
		list.add(new Notification("Server W343juuw Rebooted", "11:15:15"));
		list.add(new Notification("Product list cache refreshed", "10:32:19"));
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Alert>> getAlerts() {
		List<Alert> list = new ArrayList<>();
		list.add(new Alert("Server CPU utilization is high", "12:34:45", "text"));
		list.add(new Alert("Network latency crossed the limit", "11:15:15", "text"));
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ChartModel> getChart() {
		ChartDataDto chartDto = this.dashboardService.getChartData();
		ChartModel chartModel = new ChartModel();
		chartModel.setChartLabel(chartDto.getChartLabel());
		Map<String, String> clusterMap = chartDto.getClusterMap();
		List<ChartValue> chartValueList = new ArrayList<>();
		clusterMap.forEach((k, v) -> {
			ChartValue chartValue = new ChartValue();
			chartValue.setKey(k);
			chartValue.setValue(v);
			chartValueList.add(chartValue);
		});
		chartModel.setChartValueList(chartValueList);
		return new ResponseEntity<>(chartModel, HttpStatus.OK);

	}
}
