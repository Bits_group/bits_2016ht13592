package org.bits.dashboard.dto;

import java.util.Map;

public class ChartDataDto {
	private String chartLabel;
	private Map<String, String> clusterMap;

	public String getChartLabel() {
		return chartLabel;
	}

	public void setChartLabel(String chartLabel) {
		this.chartLabel = chartLabel;
	}

	public Map<String, String> getClusterMap() {
		return clusterMap;
	}

	public void setClusterMap(Map<String, String> clusterMap) {
		this.clusterMap = clusterMap;
	}
}
