package org.bits.dashboard.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChartModel {
	@JsonProperty
	private String chartLabel;
	
	@JsonProperty
	private List<ChartValue> chartValueList;

	public String getChartLabel() {
		return chartLabel;
	}

	public void setChartLabel(String chartLabel) {
		this.chartLabel = chartLabel;
	}

	public List<ChartValue> getChartValueList() {
		return chartValueList;
	}

	public void setChartValueList(List<ChartValue> chartValueList) {
		this.chartValueList = chartValueList;
	}	
	
}
