package org.bits.dashboard.dto;

public class Tile {
	private String tileName;
	private String text;
	private String tileColor;
	private String tileIcon;
		
	public Tile(String tileName, String text) {
		super();
		this.tileName = tileName;
		this.text = text;
	}
	
	public String getTileName() {
		return tileName;
	}
	public void setTileName(String tileName) {
		this.tileName = tileName;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTileColor() {
		return tileColor;
	}
	public void setTileColor(String tileColor) {
		this.tileColor = tileColor;
	}
	public String getTileIcon() {
		return tileIcon;
	}
	public void setTileIcon(String tileIcon) {
		this.tileIcon = tileIcon;
	}
	
	
}
