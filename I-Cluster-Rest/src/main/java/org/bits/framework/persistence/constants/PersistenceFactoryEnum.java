package org.bits.framework.persistence.constants;

public enum PersistenceFactoryEnum {
	NOSQL_FACTORY,
	RDBMS_FACTORY
}
