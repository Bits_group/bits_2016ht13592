package org.bits.framework.base;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:properties/database.properties")
public class DatabaseConfig {
	@Value("${nosql.username}")
	private String noSqlDBUserName;

	@Value("${nosql.password}")
	private String noSqlDBPassword;

	@Value("${nosql.database}")
	private String noSqlDatabaseName;

	@Value("${rdbms.username}")
	private String rdbmsDBUserName;

	@Value("${rdbms.password}")
	private String rdbmsDBPassword;

	@Value("${rdbms.database}")
	private String rdbmsDatabaseName;

	@Value("${rdbms.schema}")
	private String rdbmsSchemaName;

	public String getNoSqlDBUserName() {
		return noSqlDBUserName;
	}

	public String getNoSqlDBPassword() {
		return noSqlDBPassword;
	}

	public String getNoSqlDatabaseName() {
		return noSqlDatabaseName;
	}

	public String getRdbmsDBUserName() {
		return rdbmsDBUserName;
	}

	public String getRdbmsDBPassword() {
		return rdbmsDBPassword;
	}

	public String getRdbmsDatabaseName() {
		return rdbmsDatabaseName;
	}

	public String getRdbmsSchemaName() {
		return rdbmsSchemaName;
	}

}
