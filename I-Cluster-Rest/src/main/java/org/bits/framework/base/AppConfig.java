package org.bits.framework.base;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:properties/application.properties")
public class AppConfig {

	@Value("${encrypt_token}")
	private boolean encryptToken;
	
	@Value("${page_template}")
	private String pageTemplate;
	
	@Value("${disable_security}")
	private boolean disableSecurity;
	
	@Value("${ldap_enabled}")
	private boolean isLdapEnabled;
	
	@Value("${issuer}")
	private String tokenIssuer;
	
	@Value("${expiryinterval_secs}")
	private String expiryIntervalInSeconds;
	
	@Value("${ws_expiryinterval_secs}")
	private String wsExpiryIntervalInSeconds;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	public boolean isEncryptToken() {
		return encryptToken;
	}

	public String getPageTemplate() {
		return pageTemplate;
	}

	public boolean isDisableSecurity() {
		return disableSecurity;
	}
	
	public boolean isLdapEnabled() {
		return isLdapEnabled;
	}

	public String getTokenIssuer() {
		return tokenIssuer;
	}

	public String getExpiryIntervalInSeconds() {
		return expiryIntervalInSeconds;
	}

	public String getWsExpiryIntervalInSeconds() {
		return wsExpiryIntervalInSeconds;
	}	

	@PostConstruct
	void print(){
		System.out.println("loaded resource files");
		getTokenIssuer();
	}	
}