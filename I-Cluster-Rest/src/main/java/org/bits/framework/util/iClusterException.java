package org.bits.framework.util;

public class iClusterException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public iClusterException(String exception){
		super(exception);
	}
	
	public iClusterException(Exception exception){
		super(exception);
	}

}
