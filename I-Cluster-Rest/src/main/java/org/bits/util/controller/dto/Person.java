package org.bits.util.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

	@JsonProperty
	String name;
	
	@JsonProperty
	String age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
	
}
