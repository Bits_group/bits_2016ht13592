package org.bits.util.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Alert {
	@JsonProperty("alertName")
	private String alertName;

	@JsonProperty("alertTime")
	private String alertTime;

	@JsonProperty("alertDesc")
	private String alertDesc;

	public Alert(String alertName, String alertTime, String alertDesc) {
		super();
		this.alertName = alertName;
		this.alertTime = alertTime;
		this.alertDesc = alertDesc;
	}

	public Alert() {
	}

	public String getAlertName() {
		return alertName;
	}

	public void setAlertName(String alertName) {
		this.alertName = alertName;
	}

	public String getAlertTime() {
		return alertTime;
	}

	public void setAlertTime(String alertTime) {
		this.alertTime = alertTime;
	}

	public String getAlertDesc() {
		return alertDesc;
	}

	public void setAlertDesc(String alertDesc) {
		this.alertDesc = alertDesc;
	}

}
