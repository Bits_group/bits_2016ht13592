package org.bits.util.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ObjectList<T> {
	@JsonProperty("values")
	List<T> list;

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
	public void addItem(T item) {
		if(this.list == null) {
			list = new ArrayList<>();
		}
		list.add(item);
	}
}
