package org.bits.util.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Task {
	@JsonProperty("taskName")
	private String taskName;
	
	@JsonProperty("taskProgress")
	private String taskProgress;
	
	public Task(String taskName, String taskProgress) {
		this.taskName = taskName;
		this.taskProgress = taskProgress;
	}
	
	public Task() {}
}
