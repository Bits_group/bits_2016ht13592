package org.bits.util.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Notification {

	@JsonProperty("notificationName")
	private String notificationName;

	@JsonProperty("notificationDate")
	private String notificationDate;

	public Notification(String notificationName, String notificationDate) {
		this.notificationName = notificationName;
		this.notificationDate = notificationDate;
	}

	public Notification() {
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public String getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}

}
